import { Circle } from "./shapes/Circle";
import { Polygon } from "./shapes/Polygon";
import { Rectangle } from "./shapes/Rectangle";
import { Shape } from "./shapes/Shape";
import { ShapeReport as ShapeReporter } from "./shapes/ShapeReport";

console.log('TS-Shapes');

const shapes: Shape[] = [
  new Circle(1),
  new Rectangle(0, 3),
  new Polygon(4, 4)
];

const reporter = new ShapeReporter();

for (const shape of shapes) {
  reporter.printToConsole(shape);
}