import { Shape } from "./Shape";

export class ShapeReport {
  constructor() {}

  printToConsole(shape: Shape) {
    console.log(`Shape area = ${shape.getArea().toPrecision(3)}, perimeter = ${shape.getPerimeter().toPrecision(3)}`);
  }
}