import { Shape } from "./Shape";

export class Rectangle extends Shape {

  constructor (private width: number, private height: number) {
    super();
    this.width = Math.max(width, 0)
    this.height = Math.max(height, 0)
  }

  getPerimeter(): number {
    return (this.width + this.height) * 2;
  }
  getArea(): number {
    return this.width * this.height;
  }
  
}