import { Shape } from "./Shape";

export class Circle extends Shape {

  constructor (private radius: number) {
    super();
    this.radius = Math.max(radius, 0);
  }

  getPerimeter(): number {
    return 2 * Math.PI * this.radius;
  }
  getArea(): number {
    return Math.PI * Math.pow(this.radius, 2);
  }
  
}