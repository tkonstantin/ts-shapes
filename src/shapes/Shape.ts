export abstract class Shape {
  abstract getPerimeter(): number;
  abstract getArea(): number;
}