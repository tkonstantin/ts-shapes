import { Shape } from "./Shape";

export class Polygon extends Shape {

  constructor (private side: number, private count: number) {
    super();
    this.side = Math.max(side, 0)
    this.count = Math.max(count, 0)
  }

  getPerimeter(): number {
    return this.side * this.count;
  }
  getArea(): number {
    return (this.count * Math.pow(this.side, 2)) / (4 * Math.tan(Math.PI / this.count));
  }
  
}